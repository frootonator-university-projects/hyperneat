import random
from multiprocessing import Pool, Process, Value, Queue
from pathlib import Path
from time import time

from .activations import *
from .cppn import CPPN, Edge, Family, Node
from .dataset import Data
from .substrate import Substrate
from .utils import ROOT, RUNTIME, NodeType, Parameters, Point
from .visualization import plot_graph


class Sentinel:
    pass

class MutationProcess(Process):
    def __init__(self, name: int, input: Queue, output: Queue, current_innov: Value, params: list):
        Process.__init__(self)
        # Входная очередь
        self.input = input
        # Выходная очередь
        self.output = output
        # Историческая метка 
        self.current_innov = current_innov
        # Параметры мутации
        self.params = params
        # Номер процесса
        self.name = name
    
    def run(self):
        # Выполняем, пока не будет вызван break
        while True:
            # Считываем особь из очереди
            specie = self.input.get()
            # Проверка на выход из процесса
            if isinstance(specie, Sentinel):
                break
            # Блокируем доступ к исторической метке из других процессов
            with self.current_innov.get_lock():
                # Проводим операцию мутации
                self.current_innov.value = specie.mutate(*self.params,
                                                         self.current_innov.value)
                # Вносим новую особь в очередь
                self.output.put(specie)

class Population:
    def __init__(self, params: dict):
        self.species = []
        self.params = Parameters(params)

        self._metrics_history = {
            "fitness": [],
            "loss": [],
            "avg_loss": [],
            "accuracy": [],
            "fscore": []
        }

        self._time_execution = {
            "total": [],
            "fitness": [],
            "crossover": [],
            "mutation": []
        }

        self.__families = []
        self.__current_innov = 0
        self.__sharings = np.ones(self.params.pop_size)

        self.__generate()

    @property
    def metrics_history(self):
        return self._metrics_history

    @property
    def time_execution(self):
        return self._time_execution

    def __generate(self):
        for i in range(self.params.pop_size):
            nodes = []
            edges = []

            nodes.append(Node(0, NodeType.INPUT, Point(0, 0)))
            nodes.append(Node(1, NodeType.INPUT, Point(0, 1)))
            nodes.append(Node(2, NodeType.INPUT, Point(0, 2)))
            nodes.append(Node(3, NodeType.INPUT, Point(0, 3)))
            nodes.append(Node(4, NodeType.INPUT, Point(0, 4)))
            nodes.append(Node(5, NodeType.INPUT, Point(0, 5)))

            nodes.append(Node(6, NodeType.OUTPUT, Point(5, 1)))
            nodes[6].shuffleActivation()
            nodes[6].shuffleBias()

            nodes.append(Node(7, NodeType.OUTPUT, Point(5, 4)))
            nodes[7].shuffleActivation()
            nodes[7].shuffleBias()

            edges.append(Edge(0, 6, innov=1))
            edges.append(Edge(1, 6, innov=2))
            edges.append(Edge(2, 6, innov=3))
            edges.append(Edge(3, 6, innov=4))
            edges.append(Edge(4, 6, innov=5))
            edges.append(Edge(5, 6, innov=6))

            edges.append(Edge(0, 7, innov=7))
            edges.append(Edge(1, 7, innov=8))
            edges.append(Edge(2, 7, innov=9))
            edges.append(Edge(3, 7, innov=10))
            edges.append(Edge(4, 7, innov=11))
            edges.append(Edge(5, 7, innov=12))

            self.species.append(CPPN(nodes, edges))

        self.__current_innov = 13

    def __selection(self):
        self.species = self.species[: self.params.elite_num]
        for family in self.__families:
            king = family.species[0]
            if king[0] >= self.params.elite_num:
                self.species.append(king[1])
            if len(family.species) > 1:
                for i in range(1, len(family.species)):
                    if family.species[i][0] < self.params.elite_num:
                        continue
                    else:
                        family.species = family.species[:i]
                        break

    def __clusterize(self):
        self.__families = []
        for i in range(self.params.pop_size):
            thresh = self.params.distance_thresh
            best_family = None
            for family in self.__families:
                king = family.species[0][1]
                dist = CPPN.distance(self.species[i], king, *self.params.dist_weights)
                if dist < thresh:
                    thresh = dist
                    best_family = family

            if best_family is None:
                self.__families.append(Family(i, self.species[i]))
            else:
                best_family.add(i, self.species[i])

    def __crossover(self):
        children = []
        for family in self.__families:
            children.append(family.species[0][1])

        while len(children) < self.params.pop_size:
            family = random.choice(self.__families)
            try:
                parent_a, parent_b = [x[1] for x in random.sample(family.species, 2)]
                if random.random() < self.params.cross_prob:
                    try:
                        child = CPPN.crossover(parent_a, parent_b)
                        children.append(child)
                    except RecursionError:
                        pass
            except ValueError:
                pass

        self.species = children

    def __mutation(self):
        for specie in self.species:
            self.__current_innov = specie.mutate(
                self.params.add_node_prob,
                self.params.add_edge_prob,
                self.params.activ_prob,
                self.params.weight_prob,
                self.params.bias_prob,
                self.__current_innov
            )

    def __calcSharings(self):
        n = self.params.pop_size
        dists = np.ones((n, n))
        for i in range(n):
            for j in range(i + 1, n):
                dist = CPPN.distance(
                    self.species[i], self.species[j], *self.params.dist_weights
                )
                if dist > self.params.distance_thresh:
                    dists[i, j] = 1
                    dists[j, i] = 1
                else:
                    dists[i, j] = 0
                    dists[j, i] = 0
        for i in range(n):
            self.__sharings[i] = sum(dists[i])

    def __calcFitness(self, net: Substrate, data: Data):
        n = self.params.pop_size
        for i in range(n):
            net.setWeights(self.species[i])
            metrics = net.metrics(data)
            fitness = metrics["loss"] / self.__sharings[i]
            self.species[i].fitness = fitness
            self.species[i].loss = metrics["loss"]
            self.species[i].accuracy = metrics["accuracy"]
            self.species[i].f_score = metrics["f_score"]
    
    def calcFitness_one(self, specie: CPPN):
        # Создание копии субстрата, т.к. иначе в разных процессах будем обращаться к одному объекту,
        # пытаясь навесить разные веса
        copy_net = self.net.copy() 

        # Инициализируем веса сети в соответствии с текущей CPPN
        copy_net.setWeights(specie) 

        # Прямой проход сети и вычисление метрик
        metrics = copy_net.metrics(self.data) 

        # Записываем метрики в особь
        fitness = metrics["loss"]
        specie.fitness = fitness
        specie.loss = metrics["loss"]
        specie.accuracy = metrics["accuracy"]
        specie.f_score = metrics["f_score"]

        return specie

    def __calcFitness_pool(self, process_num: int):
        with Pool(process_num) as p:
            self.species = p.map(self.calcFitness_one, self.species)
    
    def crossover_one(self, parents: list):
        parent_a, parent_b = parents
        if random.random() < self.params.cross_prob:
            try:
                child = CPPN.crossover(parent_a, parent_b)
                return child
            except RecursionError:
                pass
    

    def __crossover_pool(self, process_num: int):
        # Создаём следующее поколение особей
        self.children = []

        # Добавляем в следующее поколение самых приспособленных
        # особей в каждом виде популяции
        for family in self.__families:
            self.children.append(family.species[0][1])
        
        # Проводим скрещивание в нескольких процессах до тех пор,
        # пока новое поколение не будет заполнено
        while len(self.children) < self.params.pop_size:
            # Вычисляем количество особей, которыми необходимо заполнить популяцию
            child_num = self.params.pop_size - len(self.children)

            # Создаём список родителей
            parents = []
            for _ in range(child_num):
                # Выбираем случайный вид
                family = random.choice(self.__families)

                # Выбираем двух случайных особей одного вида
                parent_a, parent_b = [x[1] for x in random.sample(family.species, 2)]

                # Добавляем их в список родителей
                parents.append([parent_a, parent_b])
            
            # Выполняем скрещивание в нескольких процессах
            with Pool(process_num) as p:
                self.children.extend(p.map(self.crossover_one, parents))
                self.children = [self.children[i] for i in range(len(self.children)) if self.children[i] is not None]
    
        self.species = self.children

    
    def __mutation_pool(self, process_num):
        # Инициализируем окончательный список следующего поколения
        next_generaton = []

        # Инициализируем список процессов
        processes = []

        # Инициализируем значение исторической метки
        innov_value = Value('i', self.__current_innov)

        # Очередь для исходных особей
        input_queue = Queue()

        # Очередь для особей после мутации
        output_queue = Queue()

        # Заполняем очередь особями из популяции
        for specie in self.species:
            input_queue.put(specie)
        
        # Создаём экземпляр класса, который обозначает, 
        # что все значения из очереди считаны
        sentinel = Sentinel()

        # Добавляем метку конца очереди для каждого процесса
        for _ in range(process_num):
            input_queue.put(sentinel)
        
        # Добавляем процессы в список процессов
        for i in range(process_num):
            processes.append(MutationProcess(name=str(i),
                                             input=input_queue,
                                             output=output_queue,
                                             current_innov=innov_value,
                                             params=(self.params.add_node_prob,
                                             self.params.add_edge_prob,
                                             self.params.activ_prob,
                                             self.params.weight_prob,
                                             self.params.bias_prob)))
        
        # Запускаем процессы
        for p in processes:
            p.start()

        # Ждём, когда все процессы завершатся
        for p in processes:
            p.join()
        
        # Размер дочерней популяции
        k = output_queue.qsize()    

        # Заполняем новое поколение особями из очереди
        for _ in range(k):
            value = output_queue.get()
            next_generaton.append(value)
        
        # Обновляем значение исторической метки
        self.__current_innov = innov_value.value
        self.species = next_generaton


    def parallel_evolve(self, epochs: int, net: CPPN, data: Data, process_num: int, save=False):
        # Устанавливаем очень большое число как минимальное текущее значение кросс-энтропии
        best_loss = 1e6

        # Инициализируем сеть и тренировочную выборку как члены данных класса
        self.net = net
        self.data = data

        # Создаём директорию сохранения чекпоинта и этапов формирования графа
        if save:
            save_folder = f"{ROOT}/models/{RUNTIME}"
            Path(save_folder).mkdir()

            model_save_path = f"{save_folder}/model.cppn"

            plot_folder = f"{save_folder}/graphs"
            Path(plot_folder).mkdir()
        
        # Проводим эволюционный поиск, фиксируя время, потраченное на параллельные операции
        for e in range(epochs):
            start = time()

            # Инициализация весов, прямой проход и вычисление метрик
            fitness_start = time()
            self.__calcFitness_pool(process_num)
            fitness_end = time()

            # Сортировка особей
            self.species.sort(reverse=True)
            best = self.species[0]

            # Запись метрик
            self._metrics_history["fitness"].append(best.fitness)
            self._metrics_history["loss"].append(best.loss)
            self._metrics_history["accuracy"].append(best.accuracy)
            self._metrics_history["fscore"].append(best.f_score)
            avg_loss = (
                sum([specie.loss for specie in self.species]) / self.params.pop_size
            )
            self._metrics_history["avg_loss"].append(avg_loss)

            # Вывод метрик лучшей особи на текущей итерации
            print(f"Epoch: {e + 1} Fitness:{best.fitness} CE: {best.loss}", end=" ")
            print(f"Accuracy: {best.accuracy} F1: {best.f_score} AVG_LOSS: {avg_loss}")

            # Сохранение модели, если качество улучшилось
            if best.loss < best_loss:
                best_loss = best.loss
                if save:
                    best.save(model_save_path)

            # Сохраняем граф
            if save:
                plot_graph(best, show=False, save_path=f"{plot_folder}/{e + 1}.jpg", epoch= (e + 1))
            
            # Формирование видов
            self.__clusterize()
            # Селекция особей
            self.__selection()

            # Скрещивание особей
            crossover_start = time()
            self.__crossover_pool(process_num)
            crossover_end = time()

            # Мутация особей
            mutation_start = time()
            self.__mutation_pool(process_num)
            mutation_end = time()
            end = time()

            # Запись времени вычислений
            self._time_execution['total'].append(end - start)
            self._time_execution['fitness'].append(fitness_end - fitness_start)
            self._time_execution['crossover'].append(crossover_end - crossover_start)
            self._time_execution['mutation'].append(mutation_end - mutation_start)


    def evolve(self, epochs: int, net: CPPN, train_data: Data, save=False):
        best_loss = 1e6

        if save:
            save_folder = f"{ROOT}/models/{RUNTIME}"
            Path(save_folder).mkdir()

            model_save_path = f"{save_folder}/model.cppn"

            plot_folder = f"{save_folder}/graphs"
            Path(plot_folder).mkdir()

        for e in range(epochs):
            best = self.returnBest(net, train_data)
            self._metrics_history["fitness"].append(best.fitness)
            self._metrics_history["loss"].append(best.loss)
            self._metrics_history["accuracy"].append(best.accuracy)
            self._metrics_history["fscore"].append(best.f_score)

            avg_loss = (
                sum([specie.loss for specie in self.species]) / self.params.pop_size
            )
            self._metrics_history["avg_loss"].append(avg_loss)

            print(f"Epoch: {e + 1} Fitness:{best.fitness} CE: {best.loss}", end=" ")
            print(f"Accuracy: {best.accuracy} F1: {best.f_score} AVG_LOSS: {avg_loss}")

            if best.loss < best_loss:
                best_loss = best.loss
                if save:
                    best.save(model_save_path)

            if save:
                plot_graph(best, show=False, save_path=f"{plot_folder}/{e + 1}.jpg", epoch= (e + 1))

            self.__clusterize()
            self.__selection()
            self.__crossover()
            self.__mutation()

    def returnBest(self, net: CPPN, data: Data):
        self.__calcSharings()
        self.__calcFitness(net, data)
        self.species.sort(reverse=True)
        sorted_by_loss = sorted(self.species, key=lambda x: x._loss)
        return sorted_by_loss[0]
