import pickle
from pathlib import Path
from typing import List, Tuple

import numpy as np
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score, log_loss

from .activations import *
from .cppn import CPPN
from .dataset import Data
from .utils import EPS, NodeType, Point3D


class Node(Point3D):
    def __init__(self, index: int, x: int, y: int, z: int, bias=None):
        super().__init__(x, y, z)
        self._index = index
        self.bias = bias

    @property
    def index(self):
        return self._index


class Layer:
    def __init__(self, dim: Tuple[int, int], type: NodeType, activation=None):
        self._nodes = []
        self._activation = activation
        self._type = type

        row_num, col_num = dim

        if row_num == 0 or col_num == 0:
            raise ValueError("Dims must be more than zero")

        if type == NodeType.INPUT:
            y = -1.0
            bias = None
        elif type == NodeType.HIDDEN:
            y = 0.0
            bias = 0.0
        elif type == NodeType.OUTPUT:
            y = 1.0
            bias = 0.0
        else:
            raise ValueError("Unknown type of layer:", type)

        index = 0

        if col_num != 1 and row_num != 1:
            x_h = 2.0 / (col_num - 1)
            z_h = 2.0 / (row_num - 1)
            x = -1.0
            while x < 1.0 + EPS:
                z = -1.0
                while z < 1.0 + EPS:
                    self._nodes.append(Node(index, x, y, z, bias))
                    z += z_h
                    index += 1
                x += x_h

        if col_num == 1 and row_num == 1:
            self._nodes.append(Node(index, 0, y, 0, bias))

        if col_num != 1 and row_num == 1:
            x_h = 2.0 / (col_num - 1)
            x = -1.0

            while x < 1.0 + EPS:
                self._nodes.append(Node(index, x, y, 0, bias))
                index += 1
                x += x_h

        if col_num == 1 and row_num != 1:
            z_h = 2.0 / (row_num - 1)
            z = -1.0

            while z < 1.0 + EPS:
                self._nodes.append(Node(index, 0, y, z, bias))
                index += 1
                z += z_h

    def activate(self, x: np.ndarray):
        return self._activation(x)

    @property
    def nodes(self):
        return self._nodes


class Substrate:
    def __init__(
        self,
        input_dim: Tuple[int, int],
        hidden_dim: Tuple[int, int],
        output_dim: Tuple[int, int],
        hidden_activation,
        output_activation,
    ):
        self._input = Layer(input_dim, NodeType.INPUT)
        self._hidden = Layer(hidden_dim, NodeType.HIDDEN, activation=hidden_activation)
        self._output = Layer(output_dim, NodeType.OUTPUT, activation=output_activation)

        self._input_num = input_dim[0] * input_dim[1]
        self._hidden_num = hidden_dim[0] * hidden_dim[1]
        self._output_num = output_dim[0] * output_dim[1]

        self.__input_hidden_adj = np.zeros((self._input_num, self._hidden_num))
        self.__hidden_output_adj = np.zeros((self._hidden_num, self._output_num))

        self.__input_dim = input_dim
        self.__hidden_dim = hidden_dim
        self.__output_dim = output_dim

        self.__hidden_activation = hidden_activation
        self.__output_activation = output_activation


    @property
    def input(self):
        return self._input

    @property
    def hidden(self):
        return self._hidden

    @property
    def output(self):
        return self._output

    def copy(self):
        copy = Substrate(self.__input_dim, self.__hidden_dim, self.__output_dim,
                         self.__hidden_activation, self.__output_activation)
        copy.__input_hidden_adj = self.__input_hidden_adj
        copy.__hidden_output_adj = self.__hidden_output_adj

        return copy

    def setWeights(self, cppn: CPPN):
        def forwardLayer(
            first_layer: Layer,
            second_layer: Layer,
            adj: List[List[float]],
            first_num: int,
            second_num: int,
        ):
            bias = 0
            for i in range(first_num):
                for j in range(second_num):
                    pt1 = Point3D(
                        first_layer._nodes[i].x,
                        first_layer._nodes[i].y,
                        first_layer._nodes[i].z,
                    )
                    pt2 = Point3D(
                        second_layer._nodes[j].x,
                        second_layer._nodes[j].y,
                        second_layer._nodes[j].z,
                    )
                    w, b = cppn.forward(pt1, pt2)

                    if i == 0:
                        if j == 0:
                            bias = b
                        second_layer._nodes[j].bias = bias

                    adj[i][j] = w

        forwardLayer(
            self._input,
            self._hidden,
            self.__input_hidden_adj,
            self._input_num,
            self._hidden_num,
        )

        forwardLayer(
            self._hidden,
            self._output,
            self.__hidden_output_adj,
            self._hidden_num,
            self._output_num,
        )

    def forward(self, x: np.ndarray):
        b_h = [node.bias for node in self._hidden._nodes]
        x_h = self._hidden.activate(np.matmul(x, self.__input_hidden_adj) + b_h)
        b_y = [node.bias for node in self._output._nodes]
        h_y = self._output.activate(np.matmul(x_h, self.__hidden_output_adj) + b_y)

        return h_y

    def predict(self, data: Data):
        preds = self.forward(data.features)
        pred_labels = np.argmax(preds, axis=1)
        return preds, pred_labels

    def metrics(self, data: Data):
        trues = data.target
        trues_labels = np.argmax(trues, axis=1)
        preds, pred_labels = self.predict(data)
        loss = log_loss(trues, preds)
        acc = accuracy_score(trues_labels, pred_labels)
        f1 = f1_score(trues_labels, pred_labels)
        cm = confusion_matrix(trues_labels, pred_labels)

        return {"loss": loss, "accuracy": acc, "f_score": f1, "confusion_matrix": cm}

    def save(self, save_path: str):
        if Path(save_path).suffix != ".hyperneat":
            raise Exception(
                "Incorrect extension. Substract objects must have .hyperneat suffix"
            )
        filehandler = open(save_path, "wb")
        pickle.dump(self, filehandler)

    @staticmethod
    def load(file_path: str):
        if Path(file_path).suffix != ".hyperneat":
            raise Exception(
                "Incorrect extension. Substract objects must have .hyperneat suffix"
            )
        filehandler = open(file_path, "rb")
        net = pickle.load(filehandler)
        return net
