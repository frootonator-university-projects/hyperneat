import math as m

import numpy as np


# Для CPPN
def bipolar_sigmoid(x: float):
    return 2.0 / (1.0 + m.exp(-4.9 * x)) - 1


def conditional_linear(x: float):
    return max(-1, min(1, x))


def gaussian(x: float):
    return m.exp(-((2.5 * x) ** 2))


def sinusoid(x: float):
    return m.sin(2 * x)


# Для субстрата
def np_sigmoid(x: np.ndarray):
    return 1.0 / (1.0 + np.exp(-x))


def np_tanh(x: np.ndarray):
    return 2 * np_sigmoid(2 * x) - 1.0


def np_softmax(x: np.ndarray):
    return np.divide(x, np.sum(x, axis=1)[:, None])


def np_relu(x: np.ndarray):
    return np.maximum(x, 0)


activations = [bipolar_sigmoid, conditional_linear, gaussian, sinusoid]
