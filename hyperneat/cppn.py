import pickle
import random
from pathlib import Path
from typing import List, Set

import numpy as np

from .activations import *
from .utils import NodeType, Point, Point3D


class Node:
    def __init__(self, index: int, type: NodeType, pos: Point):
        self._index = index
        self._type = type
        self._pos = pos

        self.value = 0
        self.bias = 0
        self.activation = None

    def __hash__(self):
        return hash(self._index)

    def __lt__(self, other):
        return self._index < other._index

    def __le__(self, other):
        return self._index <= other._index

    def __eq__(self, other):
        return self._index == other._index

    def __ne__(self, other):
        return self._index != other._index

    def __gt__(self, other):
        return self._index > other._index

    def __ge__(self, other):
        return self._index >= other._index

    def copy(self):
        copyNode = Node(self._index, self._type, self._pos)
        if self._type != NodeType.INPUT:
            copyNode.activation = self.activation
            copyNode.bias = self.bias

        return copyNode

    @property
    def index(self):
        return self._index

    @property
    def type(self):
        return self._type

    @property
    def pos(self):
        return self._pos

    def shuffleActivation(self):
        self.activation = random.choice(activations)

    def shuffleBias(self):
        self.bias += random.uniform(-1, 1)


class Edge:
    def __init__(
        self,
        src: int,
        dst: int,
        innov: int,
        weight: float = random.uniform(-1, 1),
        enabled: bool = True,
    ):
        self._src = src
        self._dst = dst
        self._innov = innov

        self.weight = weight
        self.enabled = enabled

    def __repr__(self):
        return str((self._src, self._dst, self._innov, self.weight, self.enabled))

    def __lt__(self, other):
        return self._innov < other._innov

    def __le__(self, other):
        return self._innov <= other._innov

    def __eq__(self, other):
        return self._innov == other._innov

    def __ne__(self, other):
        return self._innov != other._innov

    def __gt__(self, other):
        return self._innov > other._innov

    def __ge__(self, other):
        return self._innov >= other._innov

    def copy(self):
        return Edge(self._src, self._dst, self._innov, self.weight, self.enabled)

    @property
    def src(self):
        return self._src

    @property
    def dst(self):
        return self._dst

    @property
    def innov(self):
        return self._innov

    def shuffleWeight(self):
        self.weight += random.uniform(-1, 1)


class CPPN:
    def __init__(self, nodes: List[Node] = [], edges: List[Edge] = []):
        self._nodes = nodes
        self._edges = edges

        self._inputs = [
            node.index for node in self._nodes if node.type == NodeType.INPUT
        ]
        self._hidden = [
            node.index for node in self._nodes if node.type == NodeType.HIDDEN
        ]
        self._outputs = [
            node.index for node in self._nodes if node.type == NodeType.OUTPUT
        ]

        self._fitness = None
        self._loss = None
        self._accuracy = None
        self._f_score = None

        self.__adjacency = None
        self.__sorted = None

        self.__updateStructure()

    def __lt__(self, other):
        return self._fitness > other._fitness

    def __le__(self, other):
        return self._fitness >= other._fitness

    def __eq__(self, other):
        return self._fitness == other._fitness

    def __ne__(self, other):
        return self._fitness != other._fitness

    def __gt__(self, other):
        return self._fitness < other._fitness

    def __ge__(self, other):
        return self._fitness <= other._fitness

    def getEdgeByNodes(self, src: int, dst: int):
        for edge in self._edges:
            if edge.src == src and edge.dst == dst:
                return edge

        raise Exception("No such edge")

    def getNodesByEdge(self, edge: Edge):
        src, dst = edge.src, edge.dst
        node1, node2 = None, None
        for node in self._nodes:
            if node.index == src:
                node1 = node
            if node.index == dst:
                node2 = node
            if node1 and node2:
                break

        return node1, node2

    def getEdgeByInnov(self, innov: int):
        for edge in self._edges:
            if edge.innov == innov:
                return edge
        raise Exception("No such edge")

    def getNodeByIndex(self, index: int):
        for node in self._nodes:
            if node.index == index:
                return node
        raise Exception("No such node")

    def __addNode(self, edge: Edge, innov: int):
        node1, node2 = self.getNodesByEdge(edge)

        x = abs(node1.pos.x - node2.pos.x) / 2 + random.random()
        y = random.choice([node1, node2]).pos.y + random.uniform(-1, 1)

        newNode = Node(len(self._nodes), NodeType.HIDDEN, Point(x, y))
        newNode.shuffleActivation()
        newNode.shuffleBias()

        edge.enabled = False

        innov += 1
        newEdge1 = Edge(edge.src, newNode.index, innov)

        innov += 1
        newEdge2 = Edge(newNode.index, edge.dst, innov)

        self._edges.append(newEdge1)
        self._edges.append(newEdge2)
        self._nodes.append(newNode)
        self._hidden.append(newNode.index)

    def __addEdge(self, node1: Node, node2: Node, innov: int):
        innov += 1
        newEdge = Edge(node1.index, node2.index, innov)

        self._edges.append(newEdge)

    def __updateStructure(self):
        n = len(self._nodes)
        self.__adjacency = [[] for _ in range(n)]

        for edge in self._edges:
            self.__adjacency[edge.dst].append(edge.src)

        visited = set()
        self.__sorted = []

        for i in range(n):
            self.__sort(i, visited)

    def __sort(self, node: int, visited: Set[int]):
        if node in visited:
            return 0

        for adj in self.__adjacency[node]:
            self.__sort(adj, visited)

        visited.add(node)
        self.__sorted.append(node)

    def forward(self, pt1: Point3D, pt2: Point3D):
        self._nodes[0].value = pt1.x
        self._nodes[1].value = pt1.y
        self._nodes[2].value = pt1.z
        self._nodes[3].value = pt2.x
        self._nodes[4].value = pt2.y
        self._nodes[5].value = pt2.z

        for dst in self.__sorted:
            sum = 0
            if self._nodes[dst].type != NodeType.INPUT:
                for src in self.__adjacency[dst]:
                    edge = self.getEdgeByNodes(src, dst)
                    if edge.enabled:
                        sum += edge.weight * self._nodes[src].value
                self._nodes[dst].value = self._nodes[dst].activation(
                    sum + self._nodes[dst].bias
                )
        output = [self._nodes[i].value for i in self._outputs]
        return output

    def mutate(
        self,
        prob_node: float,
        prob_edge: float,
        prob_activ: float,
        prob_weight: float,
        prob_bias: float,
        innov: float,
    ):
        if random.random() < prob_node:
            edge = random.choice([x for x in self._edges if x.enabled])
            self.__addNode(edge, innov)
            innov += 2

            try:
                self.__updateStructure()
            except RecursionError:
                self._nodes.pop()
                self._edges.pop()
                self._edges.pop()

                edge.enabled = True
                innov -= 2
                self.__updateStructure()

        if random.random() < prob_edge:
            nodes = random.sample(self._nodes, 2)
            nodes.sort()
            src, dst = nodes[0].index, nodes[1].index
            flag = True

            if nodes[0].type != NodeType.HIDDEN and nodes[1].type != NodeType.HIDDEN:
                flag = False
            elif nodes[0].type == NodeType.OUTPUT or nodes[1].type == NodeType.INPUT:
                nodes[0], nodes[1] = nodes[1], nodes[0]

            src, dst = nodes[0].index, nodes[1].index
            if src in self.__adjacency[dst] or dst in self.__adjacency[src]:
                flag = False

            if flag:
                self.__addEdge(nodes[0], nodes[1], innov)
                innov += 1

            try:
                self.__updateStructure()
            except RecursionError:
                self._edges.pop()
                innov -= 1
                self.__updateStructure()

        if random.random() < prob_activ:
            if len(self._hidden) > 0:
                index = random.choice(self._hidden)
                node = self.getNodeByIndex(index)
                node.shuffleActivation()

        if random.random() < prob_weight:
            edge = random.choice([x for x in self._edges if x.enabled])
            edge.shuffleWeight()

        if random.random() < prob_bias:
            node = random.choice([x for x in self._nodes])
            node.shuffleBias()

        return innov

    @property
    def nodes(self):
        return self._nodes

    @property
    def edges(self):
        return self._edges

    @property
    def loss(self):
        return self._loss

    @loss.setter
    def loss(self, value):
        self._loss = value

    @property
    def fitness(self):
        return self._fitness

    @fitness.setter
    def fitness(self, value):
        self._fitness = value

    @property
    def accuracy(self):
        return self._accuracy

    @accuracy.setter
    def accuracy(self, value):
        self._accuracy = value

    @property
    def f_score(self):
        return self._f_score

    @f_score.setter
    def f_score(self, value):
        self._f_score = value

    @staticmethod
    def distance(
        parent_a, parent_b, delta1: float, delta2: float, delta3: float, delta4: float
    ):
        edges_a = parent_a.edges
        edges_b = parent_b.edges

        nodes_a = parent_a.nodes
        nodes_b = parent_b.nodes

        t1 = 0  # excess
        t2 = 0  # disjoint
        t3 = 0  # weight
        t4 = 0  # activation

        min_edges = min(len(edges_a), len(edges_b))
        max_edges = max(len(edges_a), len(edges_b))

        t1 = (max_edges - min_edges) / max_edges

        disjoint = 0
        for i in range(min_edges):
            if edges_a[i] not in edges_b:
                disjoint += 1

        t2 = disjoint / max_edges

        matching = 0
        for edge in edges_a:
            if edge in edges_b:
                matching += 1
                edge_b = parent_b.getEdgeByInnov(edge.innov)
                t3 += abs(edge.weight - edge_b.weight)
        if matching:
            t3 /= matching

        n_nodes = min(len(nodes_a), len(nodes_b))
        for i in range(n_nodes):
            if nodes_a[i].type != NodeType.INPUT and nodes_b[i].type != NodeType.INPUT:
                if nodes_a[i].activation != nodes_b[i].activation:
                    t4 += 1

        t4 /= n_nodes

        return t1 * delta1 + t2 * delta2 + t3 * delta3 + t4 * delta4

    @staticmethod
    def crossover(parent1, parent2):
        new_edges = []
        new_nodes = set()

        if parent1.fitness > parent2.fitness:
            parent_a, parent_b = parent1, parent2
        else:
            parent_a, parent_b = parent2, parent1

        edges_a = parent_a.edges
        edges_b = parent_b.edges

        edges_a.sort()
        edges_b.sort()

        for edge in edges_a:
            if edge in edges_b:
                if random.random() > 0.5:
                    edge_temp = edge
                    parent_temp = parent_a
                else:
                    i = edges_b.index(edge)
                    edge_temp = edges_b[i]
                    parent_temp = parent_b

                new_edges.append(edge_temp.copy())
                src, dst = edge_temp.src, edge_temp.dst
                node1, node2 = parent_temp.getNodeByIndex(
                    src
                ), parent_temp.getNodeByIndex(dst)
                new_nodes.add(node1.copy())
                new_nodes.add(node2.copy())

            else:
                new_edges.append(edge.copy())
                src, dst = edge.src, edge.dst
                node1, node2 = parent_a.getNodeByIndex(src), parent_a.getNodeByIndex(
                    dst
                )
                new_nodes.add(node1.copy())
                new_nodes.add(node2.copy())

        new_nodes = list(new_nodes)
        new_edges.sort()
        new_nodes.sort()

        return CPPN(new_nodes, new_edges)

    def save(self, save_path: str):
        if Path(save_path).suffix != ".cppn":
            raise Exception("Incorrect extension. CPPN objects must have .cppn suffix")
        filehandler = open(save_path, "wb")
        pickle.dump(self, filehandler)

    @staticmethod
    def load(file_path: str):
        filehandler = open(file_path, "rb")
        net = pickle.load(filehandler)
        return net


class Family:
    def __init__(self, index: int, specie: CPPN):
        self.species = [(index, specie)]

    def add(self, index: int, specie: CPPN):
        self.species.append((index, specie))
        self.species.sort(key=lambda x: x[1], reverse=True)
