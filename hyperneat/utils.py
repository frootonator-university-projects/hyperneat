from datetime import datetime
from enum import Enum
from pathlib import Path

EPS = 1e-5
ROOT = Path(__file__).parents[1]
RUNTIME = datetime.now().strftime("%d_%m_%Y_%H:%M:%S")


class NodeType(Enum):
    INPUT = (1,)
    HIDDEN = (2,)
    OUTPUT = 3


class Parameters:
    def __init__(self, params: dict):
        self.pop_size = params["pop_size"]
        self.cross_prob = params["cross_prob"]
        self.add_node_prob = params["add_node_prob"]
        self.add_edge_prob = params["add_edge_prob"]
        self.activ_prob = params["activ_prob"]
        self.weight_prob = params["weight_prob"]
        self.bias_prob = params["bias_prob"]
        self.selection_thresh = params["selection_thresh"]
        self.distance_thresh = params["distance_thresh"]
        self.dist_weights = [
            params["weight_excess"],
            params["weight_disjoint"],
            params["weight_node_diff"],
            params["weight_node_fun"],
        ]

        self.elite_num = int(self.selection_thresh * self.pop_size)


class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class Point3D(Point):
    def __init__(self, x: float, y: float, z: float):
        super().__init__(x, y)
        self.z = z
