from typing import List

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.art3d as art3d
import networkx as nx
import numpy as np
from matplotlib.patches import Rectangle
from sklearn.metrics import ConfusionMatrixDisplay

from .activations import *
from .cppn import CPPN
from .substrate import Substrate
from .utils import ROOT, NodeType


def plot_graph(cppn: CPPN, show: bool = True, save_path: str = None, epoch = None):
    fig1 = plt.figure()
    ax1 = fig1.add_subplot()
    edges = []

    img_bs = mpimg.imread(f"{ROOT}/hyperneat/imgs/bipolar_sigmoid.png")
    img_cl = mpimg.imread(f"{ROOT}/hyperneat/imgs/conditional_linear.png")
    img_g = mpimg.imread(f"{ROOT}/hyperneat/imgs/gaussian.png")
    img_s = mpimg.imread(f"{ROOT}/hyperneat/imgs/sinusoid.png")
    img_i = mpimg.imread(f"{ROOT}/hyperneat/imgs/input.png")

    for edge in cppn.edges:
        if edge.enabled:
            edges.append((edge.src, edge.dst))
    G = nx.DiGraph()
    G.add_edges_from(edges)
    for node in cppn.nodes:
        i = node.index
        try:
            if node.activation == bipolar_sigmoid:
                img = img_bs
            elif node.activation == conditional_linear:
                img = img_cl
            elif node.activation == gaussian:
                img = img_g
            elif node.activation == sinusoid:
                img = img_s
            elif node.type == NodeType.INPUT:
                img = img_i

            G.nodes[i]["image"] = img
            G.nodes[i]["pos"] = [node.pos.x, node.pos.y]
        except KeyError:
            pass

    pos = nx.get_node_attributes(G, "pos")
    nx.draw(G, pos, node_color="white", node_size=300)

    tr_figure = ax1.transData.transform
    tr_axes = fig1.transFigure.inverted().transform

    icon_size = (ax1.get_xlim()[1] - ax1.get_xlim()[0]) * 0.02
    icon_center = icon_size / 2.0

    if epoch:
        plt.title(f"Epoch: {epoch}")

    for n in G.nodes:
        xf, yf = tr_figure(pos[n])
        xa, ya = tr_axes((xf, yf))
        a = plt.axes([xa - icon_center, ya - icon_center, icon_size, icon_size])
        a.imshow(G.nodes[n]["image"])
        a.axis("off")

    if save_path:
        plt.savefig(save_path)

    if show:
        plt.show()
    else:
        plt.close()


def plot_substrate(net: Substrate, show: bool = True, save_path: str = None):
    input_pts = np.array([[node.x, node.y, node.z] for node in net.input.nodes])
    hidden_pts = np.array([[node.x, node.y, node.z] for node in net.hidden.nodes])
    output_pts = np.array([[node.x, node.y, node.z] for node in net.output.nodes])

    connections = []

    for input in input_pts:
        for hidden in hidden_pts:
            connections.append([*input, *hidden])

    for hidden in hidden_pts:
        for output in output_pts:
            connections.append([*hidden, *output])

    X, Y, Z, U, W, V = zip(*connections)

    fig = plt.figure()
    ax = fig.add_subplot(projection="3d")

    ax.scatter3D(
        input_pts[:, 0], input_pts[:, 1], input_pts[:, 2], s=50, color="darkcyan"
    )
    ax.scatter3D(
        hidden_pts[:, 0], hidden_pts[:, 1], hidden_pts[:, 2], color="darkgreen"
    )
    ax.scatter3D(
        output_pts[:, 0], output_pts[:, 1], output_pts[:, 2], s=50, color="darkorchid"
    )

    input_layer = Rectangle(xy=(-1, -1), width=2, height=2, color="darkcyan", alpha=0.3)
    hidden_layer = Rectangle(
        xy=(-1, -1), width=2, height=2, color="darkgreen", alpha=0.3
    )
    output_layer = Rectangle(
        xy=(-1, -1), width=2, height=2, color="darkorchid", alpha=0.3
    )

    ax.add_patch(input_layer)
    art3d.pathpatch_2d_to_3d(input_layer, z=-1, zdir="y")
    ax.add_patch(hidden_layer)
    art3d.pathpatch_2d_to_3d(hidden_layer, z=0, zdir="y")
    ax.add_patch(output_layer)
    art3d.pathpatch_2d_to_3d(output_layer, z=1, zdir="y")

    ax.text(
        -1,
        -1,
        1.2,
        "Входной слой",
        backgroundcolor="black",
        color="white",
        fontsize="x-small",
    )
    ax.text(
        -1,
        0,
        1.2,
        "Скрытый слой",
        backgroundcolor="black",
        color="white",
        fontsize="x-small",
    )
    ax.text(
        -1,
        1,
        1.2,
        "Выходной слой",
        backgroundcolor="black",
        color="white",
        fontsize="x-small",
    )

    ax.set_title("Субстрат")
    ax.grid(False)
    for input in input_pts:
        for hidden in hidden_pts:
            ax.plot(
                [input[0], hidden[0]],
                [input[1], hidden[1]],
                [input[2], hidden[2]],
                color="black",
                alpha=0.3,
                linewidth=0.5,
            )

    for hidden in hidden_pts:
        for output in output_pts:
            ax.plot(
                [hidden[0], output[0]],
                [hidden[1], output[1]],
                [hidden[2], output[2]],
                color="black",
                alpha=0.3,
                linewidth=0.5,
            )

    if save_path:
        plt.savefig(save_path)

    if show:
        plt.show()
    else:
        plt.close()


def plot_metrics(pop, show: bool = True, save_path: str = None):
    fig2, ax2 = plt.subplots(2, 3, figsize=(15, 8))

    fitness = pop.metrics_history["fitness"]
    loss = pop.metrics_history["loss"]
    accuracy = pop.metrics_history["accuracy"]
    fscore = pop.metrics_history["fscore"]
    avg_loss = pop.metrics_history["avg_loss"]

    epochs = list(range(len(fitness)))

    ax2[0, 0].plot(epochs, fitness, color="darkcyan")
    ax2[0, 0].set_title("Fitness")

    ax2[0, 1].plot(epochs, loss, color="darkcyan")
    ax2[0, 1].set_title("Cross-Entropy")

    ax2[0, 2].plot(epochs, avg_loss, color="darkcyan")
    ax2[0, 2].set_title("Average Cross-Entropy")

    ax2[1, 0].plot(epochs, accuracy, color="darkcyan")
    ax2[1, 0].set_title("Accuracy")

    ax2[1, 1].set_visible(False)

    ax2[1, 2].plot(epochs, fscore, color="darkcyan")
    ax2[1, 2].set_title("F-score")

    fig2.tight_layout(pad=2)

    for row in ax2:
        for el in row:
            el.grid()

    plt.setp(ax2, xlabel="epochs")

    if save_path:
        plt.savefig(save_path)

    if show:
        plt.show()
    else:
        plt.close()


def plot_confusion_matrix(cm: List[int]):
    fig3 = ConfusionMatrixDisplay(cm)
    fig3.plot()
    plt.show()


def plot_time_execution(pop, save_path=None, show=False):
    fig2, ax2 = plt.subplots(2, 2, figsize=(15, 8))

    total = pop.time_execution["total"]
    fitness = pop.time_execution["fitness"]
    crossover = pop.time_execution["crossover"]
    mutation = pop.time_execution["mutation"]

    epochs = list(range(len(fitness)))

    ax2[0, 0].plot(epochs, total, color="darkcyan")
    ax2[0, 0].set_title("Total")

    ax2[0, 1].plot(epochs, fitness, color="darkcyan")
    ax2[0, 1].set_title("Calculations")

    ax2[1, 0].plot(epochs, crossover, color="darkcyan")
    ax2[1, 0].set_title("Crossover")

    ax2[1, 1].plot(epochs, mutation, color="darkcyan")
    ax2[1, 1].set_title("Mutation")

    fig2.tight_layout(pad=2)

    for row in ax2:
        for el in row:
            el.grid()

    plt.setp(ax2, xlabel="epochs")

    if save_path:
        plt.savefig(save_path)

    if show:
        plt.show()
    else:
        plt.close()